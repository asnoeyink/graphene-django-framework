Mutations
=========

There are builtin mutations to easily handle the simple
create, update, and delete operations typically
found in applications.

Create
------

.. code-block:: python

    class CreateUserMutation(CreateMutation)
        class Meta:
            model = User


.. class:: CreateMutation

    something goes here

    .. method:: mutate
        :return: int


Update
------

.. code-block:: python

    class UpdateUserMutation(UpdateMutation)
        class Meta:
            model = User


Delete
------

.. code-block:: python

    class DeleteUserMutation(DeleteMutation)
        class Meta:
            model = User


Action
------

.. code-block:: python

    class SendUserEmailMutation(ActionMutation)
        class Meta:
            model = User

        @classmethod
        def(cls, root, info, **kwargs):
            #do mutation
            return SendUserEmailMutation()

