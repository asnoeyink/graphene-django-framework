
from django.apps import apps
from django.test import TestCase, RequestFactory
from graphene.test import Client

from ..schema import schema


class ModelFilterFieldTests(TestCase):
    """ Tests for ModelFilterField. """

    @classmethod
    def setUpTestData(cls):
        super(ModelFilterFieldTests, cls).setUpTestData()

        User = apps.get_model('auth.User')

        cls.test_client = Client(schema=schema)
        cls.test_request = RequestFactory()
        cls.test_request.user = User.objects.create(username='superduperuser', is_superuser=True)

        cls.test_user_1 = User.objects.create(username='test_1', is_staff=True)
        cls.test_user_2 = User.objects.create(username='test_2', is_staff=True)
        cls.test_user_3 = User.objects.create(username='test_3', is_staff=True)

        cls.test_user_4 = User.objects.create(username='test_4', first_name='James', is_staff=False)
        cls.test_user_5 = User.objects.create(username='test_5', first_name='James', is_staff=False)

    def test_filter_field(self):
        """ ModelFilterField will return a list of type instances that match certain criteria. """
        result = self.test_client.execute(''' { filterUsers(firstName:"James") {
        username
        }}
        ''', context=self.test_request)

        expected = {
            "data": {
                "filterUsers": [
                    {
                        "username": "{}".format(self.test_user_4)
                    },
                    {
                        "username": "{}".format(self.test_user_5)
                    }
                ]
            }
        }

        self.assertEqual(len(result['data']['filterUsers']), 2)
        self.assertEqual(result['data']['filterUsers'],
                         expected['data']['filterUsers'])

    def test_filter_field_with_filterset(self):
        """ ModelFilterField will return a list of type instances that match certain criteria.
            It will use a passed FilterSet when filtering the list.
        """
        result = self.test_client.execute(''' { filterUsersCustomFilterset(isStaff:true) {
        username
        }}
        ''', context=self.test_request)

        expected = {
            "data": {
                "filterUsersCustomFilterset": [
                    {
                        "username": "{}".format(self.test_user_1)
                    },
                    {
                        "username": "{}".format(self.test_user_2)
                    },
                    {
                        "username": "{}".format(self.test_user_3)
                    }
                ]
            }
        }

        self.assertEqual(len(result['data']['filterUsersCustomFilterset']), 3)
        self.assertEqual(result['data']['filterUsersCustomFilterset'],
                         expected['data']['filterUsersCustomFilterset'])

