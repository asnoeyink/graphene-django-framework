from django.contrib.auth.models import ContentType, User, Permission
from django.core.exceptions import PermissionDenied
from django.test import TestCase, RequestFactory
from graphene.types.context import Context

from graphene_django_framework.decorators import needs_permission, needs_module_permissions, only_allow


class OnlyAllowDecoratorTests(TestCase):
    """ Tests for only_allow() . """

    @only_allow(superuser=True)
    def superuser_func(self, info, **kwargs):
        """ Only superusers should be able to access this function. """
        return 'Only superusers!'

    @only_allow(staff=True)
    def staff_func(self, info, **kwargs):
        """ Only staff users should be able to access this function. """
        return 'Only staff!'

    @only_allow(superuser=True, staff=True)
    def multiple_user_func(self, info, **kwargs):
        """ Only superusers and staff users should be able to access this function. """
        return 'Only superusers and staff!'

    @classmethod
    def setUpTestData(cls):
        super(OnlyAllowDecoratorTests, cls).setUpTestData()

        # Create dummy info objects
        cls.test_superuser_info = RequestFactory()
        cls.test_staff_user_info = RequestFactory()
        cls.test_anonymous_user_info = RequestFactory()

        # Add the context variable to the info objects with a specific user type
        cls.test_superuser_info.context = Context(user=User.objects.create(username='pb22', is_superuser=True))
        cls.test_staff_user_info.context = Context(user=User.objects.create(username='bp22', is_staff=True))
        cls.test_anonymous_user_info.context = Context(user=User.objects.create(username='anonuser', is_superuser=False,
                                                                                is_active=False, is_staff=False))

    def test_only_allow_superusers(self):
        """ Only superusers should be allowed to access functions wrapped with the only_allow decorator,
            when the superuser kwarg is passed to the decorator.
        """
        allowed_user_result = self.superuser_func(self.test_superuser_info)
        self.assertEqual(allowed_user_result, 'Only superusers!')

        with self.assertRaises(PermissionDenied):
            self.superuser_func(self.test_staff_user_info)

        with self.assertRaises(PermissionDenied):
            self.superuser_func(self.test_anonymous_user_info)

    def test_only_allow_staff_users(self):
        """ Only staff users should be allowed to access functions wrapped with the only_allow decorator,
            when the staff kwarg is passed to the decorator.
        """
        allowed_user_result = self.staff_func(self.test_staff_user_info)
        self.assertEqual(allowed_user_result, 'Only staff!')

        with self.assertRaises(PermissionDenied):
            self.staff_func(self.test_anonymous_user_info)

        with self.assertRaises(PermissionDenied):
            self.staff_func(self.test_superuser_info)

    def test_only_allow_multiple_users(self):
        """ Only staff and superusers should be allowed to access functions wrapped with the only_allow decorator,
            when the staff and superuser kwargs are passed to the decorator.
        """
        allowed_user_result = self.multiple_user_func(self.test_staff_user_info)
        allowed_user_result_2 = self.multiple_user_func(self.test_superuser_info)
        self.assertEqual(allowed_user_result, 'Only superusers and staff!')
        self.assertEqual(allowed_user_result_2, 'Only superusers and staff!')

        with self.assertRaises(PermissionDenied):
            self.multiple_user_func(self.test_anonymous_user_info)


class NeedsPermissionDecoratorTests(TestCase):
    """ Tests for needs_permission() . """

    @needs_permission('auth.delete_appmodel')
    def func_requires_one_perm(self, info, **kwargs):
        """ Users with the 'auth.delete_appmodel' Permission should be able to access this function. """
        return 'Allowed with one perm!'

    @needs_permission('auth.add_appmodel', 'auth.change_appmodel', 'auth.view_appmodel')
    def func_requires_multiple_perms(self, info, **kwargs):
        """ Users with the 'auth.add_appmodel', 'auth.change_appmodel', and 'auth.view_appmodel'
            Permissions should be able to access this function.
        """
        return 'Allowed with multiple perms!'

    @classmethod
    def setUpTestData(cls):
        super(NeedsPermissionDecoratorTests, cls).setUpTestData()

        # Permissions
        content_type = ContentType.objects.get(model='permission')  # ContentType's app_label = auth
        cls.test_permission = Permission.objects.create(content_type=content_type, codename='add_appmodel')
        cls.test_permission_2 = Permission.objects.create(content_type=content_type, codename='change_appmodel')
        cls.test_permission_3 = Permission.objects.create(content_type=content_type, codename='delete_appmodel')
        cls.test_permission_4 = Permission.objects.create(content_type=content_type, codename='view_appmodel')

        # Users
        cls.test_user_with_permissions = User.objects.create(username='bob1', is_active=True)
        cls.test_user_with_permissions.user_permissions.add(cls.test_permission)
        cls.test_user_with_permissions.user_permissions.add(cls.test_permission_2)
        cls.test_user_with_permissions.user_permissions.add(cls.test_permission_3)
        cls.test_user_with_permissions.user_permissions.add(cls.test_permission_4)

        cls.test_user_with_some_permissions = User.objects.create(username='bob2', is_active=True)
        cls.test_user_with_some_permissions.user_permissions.add(cls.test_permission)
        cls.test_user_with_some_permissions.user_permissions.add(cls.test_permission_2)

        cls.test_user_without_permissions = User.objects.create(username='bob3', is_active=True)

        # Requests & Contexts
        cls.test_info_user_has_perms = RequestFactory()
        cls.test_info_user_has_no_perms = RequestFactory()
        cls.test_info_user_has_some_perms = RequestFactory()
        cls.test_info_user_has_perms.context = Context(user=cls.test_user_with_permissions)
        cls.test_info_user_has_no_perms.context = Context(user=cls.test_user_without_permissions)
        cls.test_info_user_has_some_perms.context = Context(user=cls.test_user_with_some_permissions)

    def test_needs_permission_one_perm_required(self):
        """ Users with the required Permission should be allowed access to functions wrapped with the needs_permission()
            decorator when only one Permission is required.
        """
        result_with_perms = self.func_requires_one_perm(self.test_info_user_has_perms)

        self.assertEqual(result_with_perms, 'Allowed with one perm!')

        # User doesn't have the right Permission
        with self.assertRaises(PermissionDenied):
            self.func_requires_one_perm(self.test_info_user_has_some_perms)

        with self.assertRaises(PermissionDenied):
            self.func_requires_one_perm(self.test_info_user_has_no_perms)

    def test_needs_permission_multiple_perms_required(self):
        """ Users with all of the required Permissions should be allowed access to functions wrapped
            with the needs_permission() decorator when multiple Permissions are required.
        """
        result_with_perms = self.func_requires_multiple_perms(self.test_info_user_has_perms)

        self.assertEqual(result_with_perms, 'Allowed with multiple perms!')

        # Users need ALL of the required Permissions, not just a few
        with self.assertRaises(PermissionDenied):
            self.func_requires_multiple_perms(self.test_info_user_has_some_perms)

        with self.assertRaises(PermissionDenied):
            self.func_requires_multiple_perms(self.test_info_user_has_no_perms)


class NeedsModulePermissionsDecoratorTests(TestCase):
    """ Tests for needs_module_permissions() . """

    @needs_module_permissions('auth')
    def func_requires_auth_perms(self, info, **kwargs):
        """ Users with any of the 'auth' Permissions should be able to access this function. """
        return 'Allowed with auth perms!'

    @needs_module_permissions('admin')
    def func_requires_admin_perms(self, info, **kwargs):
        """ Users with any of the 'admin' Permissions should be able to access this function. """
        return 'Allowed with admin perms!'

    @classmethod
    def setUpTestData(cls):
        super(NeedsModulePermissionsDecoratorTests, cls).setUpTestData()

        content_type = ContentType.objects.get(model='logentry')  # ContentType's app_label = admin
        content_type_2 = ContentType.objects.get(model='permission')  # ContentType's app_label = auth

        # Admin Permissions
        cls.test_admin_permission = Permission.objects.create(content_type=content_type, codename='add_appmodel')
        cls.test_admin_permission_2 = Permission.objects.create(content_type=content_type, codename='change_appmodel')
        cls.test_admin_permission_3 = Permission.objects.create(content_type=content_type, codename='delete_appmodel')

        # Auth Permissions
        cls.test_auth_permission = Permission.objects.create(content_type=content_type_2, codename='add_appmodel')
        cls.test_auth_permission_2 = Permission.objects.create(content_type=content_type_2, codename='change_appmodel')
        cls.test_auth_permission_3 = Permission.objects.create(content_type=content_type_2, codename='delete_appmodel')

        # Users
        cls.test_user_with_admin_permissions = User.objects.create(username='test1', is_active=True)
        cls.test_user_with_admin_permissions.user_permissions.add(cls.test_admin_permission)

        cls.test_user_with_auth_permissions = User.objects.create(username='test2', is_active=True)
        cls.test_user_with_auth_permissions.user_permissions.add(cls.test_auth_permission)
        cls.test_user_with_auth_permissions.user_permissions.add(cls.test_auth_permission_2)

        cls.test_user_with_both_permissions = User.objects.create(username='test3', is_active=True)
        cls.test_user_with_both_permissions.user_permissions.add(cls.test_auth_permission)
        cls.test_user_with_both_permissions.user_permissions.add(cls.test_admin_permission)

        cls.test_user_without_permissions = User.objects.create(username='test4', is_active=True)

        # Requests & Contexts
        cls.test_info_user_has_admin_module_perms = RequestFactory()
        cls.test_info_user_has_auth_module_perms = RequestFactory()
        cls.test_info_user_has_both_module_perms = RequestFactory()
        cls.test_info_user_has_no_module_perms = RequestFactory()
        cls.test_info_user_has_admin_module_perms.context = Context(user=cls.test_user_with_admin_permissions)
        cls.test_info_user_has_auth_module_perms.context = Context(user=cls.test_user_with_auth_permissions)
        cls.test_info_user_has_both_module_perms.context = Context(user=cls.test_user_with_both_permissions)
        cls.test_info_user_has_no_module_perms.context = Context(user=cls.test_user_without_permissions)

    def test_needs_auth_module_permissions(self):
        """ Users with any of the 'auth' Permissions should be allowed access to functions wrapped
            with the needs_module_permissions() decorator when 'auth' Permissions are required.
        """
        result_with_perms = self.func_requires_auth_perms(self.test_info_user_has_auth_module_perms)
        result_with_multiple_perms = self.func_requires_auth_perms(self.test_info_user_has_both_module_perms)

        self.assertEqual(result_with_perms, 'Allowed with auth perms!')
        # User has Permissions in both modules
        self.assertEqual(result_with_multiple_perms, 'Allowed with auth perms!')

        with self.assertRaises(PermissionDenied):
            self.func_requires_auth_perms(self.test_info_user_has_admin_module_perms)

        with self.assertRaises(PermissionDenied):
            self.func_requires_auth_perms(self.test_info_user_has_no_module_perms)

    def test_needs_admin_module_permissions(self):
        """ Users with any of the 'admin' Permissions should be allowed access to functions wrapped
            with the needs_module_permissions() decorator when 'admin' Permissions are required.
        """
        result_with_perms = self.func_requires_admin_perms(self.test_info_user_has_admin_module_perms)
        result_with_multiple_perms = self.func_requires_admin_perms(self.test_info_user_has_both_module_perms)

        self.assertEqual(result_with_perms, 'Allowed with admin perms!')
        # User has Permissions in both modules
        self.assertEqual(result_with_multiple_perms, 'Allowed with admin perms!')

        with self.assertRaises(PermissionDenied):
            self.func_requires_admin_perms(self.test_info_user_has_auth_module_perms)

        with self.assertRaises(PermissionDenied):
            self.func_requires_admin_perms(self.test_info_user_has_no_module_perms)
