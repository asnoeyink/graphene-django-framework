from django.conf.urls import url
from django.contrib import admin
from graphene_django.views import GraphQLView

from app.schema import schema
from graphene_django_framework.sites import site

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^site/', site.urls),
    url(r'^graphql/', GraphQLView.as_view(graphiql=True, schema=schema)),
]
